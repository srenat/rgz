<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AuthorRequest\BulkDestroyAuthorRequest;
use App\Http\Requests\Admin\AuthorRequest\DestroyAuthorRequest;
use App\Http\Requests\Admin\AuthorRequest\IndexAuthorRequest;
use App\Http\Requests\Admin\AuthorRequest\StoreAuthorRequest;
use App\Http\Requests\Admin\AuthorRequest\UpdateAuthorRequest;
use App\Models\AuthorRequest;
use Brackets\AdminAuth\Models\AdminUser;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use GuzzleHttp\Psr7\Request;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AuthorRequestsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexAuthorRequest $request
     * @return array|Factory|View
     */
    public function index(IndexAuthorRequest $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(AuthorRequest::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'user_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        foreach( $data as $item ){
            $item['full_name'] = AdminUser::where('id', $item['user_id'])->first()->full_name;
        }

        return view('admin.author-request.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.author-request.create');

        return view('admin.author-request.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAuthorRequest $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreAuthorRequest $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the AuthorRequest
        $authorRequest = AuthorRequest::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/author-requests'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/author-requests');
    }

    /**
     * Display the specified resource.
     *
     * @param AuthorRequest $authorRequest
     * @throws AuthorizationException
     * @return void
     */
    public function show(AuthorRequest $authorRequest)
    {
        $this->authorize('admin.author-request.show', $authorRequest);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AuthorRequest $authorRequest
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(AuthorRequest $authorRequest)
    {
        $this->authorize('admin.author-request.edit', $authorRequest);


        return view('admin.author-request.edit', [
            'authorRequest' => $authorRequest,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAuthorRequest $request
     * @param AuthorRequest $authorRequest
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateAuthorRequest $request, AuthorRequest $authorRequest)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values AuthorRequest
        $authorRequest->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/author-requests'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/author-requests');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyAuthorRequest $request
     * @param AuthorRequest $authorRequest
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyAuthorRequest $request, AuthorRequest $authorRequest)
    {
        $authorRequest->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyAuthorRequest $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyAuthorRequest $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    AuthorRequest::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    public function getAuthorAccess(AuthorRequest $request){
        $id = $_POST['id'];
        $user = AdminUser::find($request->find($id)->user_id);
        if( !$user->hasRole('Author') ){
            $user->assignRole('Author');
        }
        $request->find($id)->delete();
        return redirect()->back();
    }
}
