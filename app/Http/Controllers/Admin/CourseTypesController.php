<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CourseType\BulkDestroyCourseType;
use App\Http\Requests\Admin\CourseType\DestroyCourseType;
use App\Http\Requests\Admin\CourseType\IndexCourseType;
use App\Http\Requests\Admin\CourseType\StoreCourseType;
use App\Http\Requests\Admin\CourseType\UpdateCourseType;
use App\Models\CourseType;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CourseTypesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCourseType $request
     * @return array|Factory|View
     */
    public function index(IndexCourseType $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(CourseType::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name', 'description']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.course-type.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.course-type.create');

        return view('admin.course-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCourseType $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCourseType $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the CourseType
        $courseType = CourseType::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/course-types'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/course-types');
    }

    /**
     * Display the specified resource.
     *
     * @param CourseType $courseType
     * @throws AuthorizationException
     * @return void
     */
    public function show(CourseType $courseType)
    {
        $this->authorize('admin.course-type.show', $courseType);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CourseType $courseType
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(CourseType $courseType)
    {
        $this->authorize('admin.course-type.edit', $courseType);


        return view('admin.course-type.edit', [
            'courseType' => $courseType,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCourseType $request
     * @param CourseType $courseType
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCourseType $request, CourseType $courseType)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values CourseType
        $courseType->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/course-types'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/course-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCourseType $request
     * @param CourseType $courseType
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCourseType $request, CourseType $courseType)
    {
        $courseType->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCourseType $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCourseType $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('courseTypes')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
