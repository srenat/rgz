<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Course\BulkDestroyCourse;
use App\Http\Requests\Admin\Course\DestroyCourse;
use App\Http\Requests\Admin\Course\IndexCourse;
use App\Http\Requests\Admin\Course\StoreCourse;
use App\Http\Requests\Admin\Course\UpdateCourse;
use App\Models\Course;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use App\Models\CourseType;
use Brackets\AdminAuth\Models\AdminUser;
use That0n3guy\Transliteration\Transliteration;
use Illuminate\Support\Facades\Auth;

class CoursesController extends Controller
{

    protected $transliteration;
    function __construct()
    {
        $this->transliteration = new Transliteration();
        $this->guard = config('admin-auth.defaults.guard');
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexCourse $request
     * @return array|Factory|View
     */
    public function index(IndexCourse $request)
    {
        if( Auth::guard($this->guard)->user()->can('admin.course') && !Auth::guard($this->guard)->user()->can('admin') ){
            $data = AdminListing::create(Course::class)
                    ->modifyQuery(function($query) use ($request){
                        $query->whereHas('author', function ($subquery) {
                            $subquery->where('id', Auth::guard($this->guard)->user()->id);
                        });
                    })
                    ->attachPagination($request->currentPage)
                    ->get();
        }else{
            // create and AdminListing instance for a specific model and
            $data = AdminListing::create(Course::class)->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id', 'name', 'published', 'approved', 'created_by', 'modified_by'],

                // set columns to searchIn
                ['id', 'name', 'slug', 'description', 'certificate', 'contacts'],
            );
        }
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.course.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.course.create');
        $teachers = AdminUser::whereHas(
            'roles', function($q){
                $q->where('name', 'User');
            }
        )->get();


        if( Auth::guard($this->guard)->user()->can('admin') ){
            $authors = AdminUser::whereHas(
                'roles', function($q){
                    $q->where('name', 'Author');
                }
            )->get();
        }else{
            $authors = AdminUser::where('id', Auth::guard($this->guard)->user()->id)->get();
        }

        return view('admin.course.create',
            [
            'types' => CourseType::get(),
            'teachers' => $teachers,
            'authors' => $authors
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCourse $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCourse $request)
    {

        $type_ids = [];
        if( !empty( $request->get('type') ) ){
            $type_ids[] = $request->get('type')['id'];
        }

        $author_id = null;
        $approved = false;
        if( Auth::guard($this->guard)->user()->can('admin.course') && !Auth::guard($this->guard)->user()->can('admin') ){
            $author_id = Auth::guard($this->guard)->user()->id;
        }else{
            if( !empty( $request->get('author')) ){
                $author_id = $request->get('author')['id'];
            }
            if( !empty( $request->get('approved')) ){
                $approved = $request->get('approved');
            }
        }

        $teacher_id = null;
        if( !empty( $request->get('teacher')) ){
            $teacher_id = $request->get('teacher')['id'];
        }

        $request->merge([
            'slug'      => strtolower( $this->transliteration->clean_filename( $request->get('name') ) ),
            'type'      => $type_ids,
            'author'    => $author_id,
            'teacher'   => $teacher_id,
            'created_by' => Auth::guard($this->guard)->user()->id,
            'approved'  => $approved
        ]);

        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Course
        $course = Course::create($sanitized);
        if( !empty($course) ){
            $course->type()->attach($type_ids);
            $course->author()->attach($author_id);
            $course->teacher()->attach($teacher_id);
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/courses'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/courses');
    }

    /**
     * Display the specified resource.
     *
     * @param Course $course
     * @throws AuthorizationException
     * @return void
     */
    public function show(Course $course)
    {
        $this->authorize('admin.course.show', $course);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Course $course)
    {
        $this->authorize('admin.course.edit', $course);
        $course->type = $course->type()->get();
        $course->teacher = $course->teacher()->get();
        $course->author = $course->author()->get();
        $teachers = AdminUser::whereHas(
            'roles', function($q){
                $q->where('name', 'User');
            }
        )->get();

        if( Auth::guard($this->guard)->user()->can('admin') ){
            $authors = AdminUser::whereHas(
                'roles', function($q){
                    $q->where('name', 'Author');
                }
            )->get();
        }else{
            $authors = AdminUser::where('id', Auth::guard($this->guard)->user()->id)->get();
        }

        return view('admin.course.edit', [
            'course'    => $course,
            'types'     => CourseType::get(),
            'teachers'  => $teachers,
            'authors'   => $authors
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCourse $request
     * @param Course $course
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCourse $request, Course $course)
    {
        // Sanitize input
        // $sanitized = $request->getSanitized();
        if( count($request->all()) == 1 ){
            $sanitized = $request->getSanitized(true);
        }else{
            $type_ids = [];
            if( !empty( $request->get('type') ) ){
                if( is_array($request->get('type'))){
                    if( isset($request->get('type')['id']) ){
                        $type_ids[] = $request->get('type')['id'];
                    }else{
                        $type_ids[] = current($request->get('type'))['id'];
                    }
                }
            }

            $author_id = null;
            $approved = false;
            if( Auth::guard($this->guard)->user()->can('admin.course') && !Auth::guard($this->guard)->user()->can('admin') ){
                $author_id = Auth::guard($this->guard)->user()->id;
            }else{
                if( !empty( $request->get('author')) ){
                    if( is_array($request->get('author'))){
                        if( isset($request->get('author')['id']) ){
                            $author_id = $request->get('author')['id'];
                        }else{
                            $author_id = current($request->get('author'))['id'];
                        }
                    }
                }
                if( !empty( $request->get('approved')) ){
                    $approved = $request->get('approved');
                }
            }

            $teacher_id = null;
            if( !empty( $request->get('teacher')) ){
                if( is_array($request->get('teacher'))){
                    if( isset($request->get('teacher')['id']) ){
                        $teacher_id = $request->get('teacher')['id'];
                    }else{
                        $teacher_id = current($request->get('teacher'))['id'];
                    }
                }
            }

            $request->merge([
                'type'      => $type_ids,
                'author'    => $author_id,
                'teacher'   => $teacher_id,
                'modified_by' => Auth::guard($this->guard)->user()->id,
                'approved'  => $approved
            ]);

            // Sanitize input
            $sanitized = $request->getSanitized();

            $course->type()->sync($type_ids);
            $course->author()->sync($author_id);
            $course->teacher()->sync($teacher_id);

        }

        // Update changed values Course
        $course->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/courses'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
                'object' => $course
            ];
        }

        return redirect('admin/courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCourse $request
     * @param Course $course
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCourse $request, Course $course)
    {
        $course->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCourse $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCourse $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('courses')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
