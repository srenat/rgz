<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brackets\AdminAuth\Models\AdminUser;
use App\Http\Requests\Admin\AdminUser\StoreUser;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    function index(){
        return view('general.auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUser $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreUser $request)
    {
        // Sanitize input
        $sanitized = $request->getModifiedData();
        $sanitized['language'] = 'ru';
        // Store the AdminUser
        $adminUser = AdminUser::create($sanitized);

        // But we do have a roles, so we need to attach the roles to the adminUser
        $roles = Role::where('guard_name', 'user')->get();
        $adminUser->roles()->sync(collect($roles)->map->id->toArray());

        if ($request->ajax()) {
            return ['redirect' => url('admin/admin-users'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/');
    }
}
