<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use App\Http\Requests\Author\StoreAuthorRequest;
use Illuminate\Http\Request;
use App\Models\AuthorRequest;
use Brackets\AdminAuth\Models\AdminUser;

class AuthorController extends Controller
{
    function request(StoreAuthorRequest $request){
        $sanitized = $request->getSanitized();

        if( $user = AdminUser::find($sanitized['user_id']) ){
            if( $user->hasRole('Author') ){
                return redirect('/user/settings');
            }else{
                AuthorRequest::create($sanitized);
            }
        }
        return redirect('/user/settings');
    }
}
