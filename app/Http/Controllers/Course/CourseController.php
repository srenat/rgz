<?php

namespace App\Http\Controllers\Course;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Course;
use App\Models\CourseType;
use App\Models\FavoriteCourse;

class CourseController extends Controller
{

    protected $course_model;
    protected $course_type_model;
    protected $guard;

    public function __construct()
    {
        $this->guard = 'admin';
        $this->course_model = new Course();
        $this->course_type_model = new CourseType();
    }

    public function index(){
        $user = Auth::guard($this->guard)->user();


        if( isset($_GET['query']) && !empty($_GET['query'])){
            $courses = $this->course_model->getPaginateWithQuery($_GET['query']);
        }else{
            $courses = $this->course_model->getAllPaginate();
        }
        return view('general.course.list',
            [
                'user'      => $user,
                'courses'   => $courses
            ]
        );
    }

    public function show($slug){
        $user = Auth::guard($this->guard)->user();
        $course = $this->course_model->getCourseBySlug( $slug );
        if( $course ){
            return view('general.course.detail',
                [
                    'user'      => $user,
                    'course'    => $course
                ]
            );
        }else{
            abort(404);
        }
    }


}
