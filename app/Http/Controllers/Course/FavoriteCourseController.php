<?php

namespace App\Http\Controllers\Course;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\FavoriteCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteCourseController extends Controller
{
    protected $favorite_course_model;
    protected $course_model;
    protected $guard;

    public function __construct()
    {
        $this->guard = 'admin';
        $this->course_model = new Course();
        $this->favorite_course_model = new FavoriteCourse();
    }

    public function index(){
        $user = Auth::guard($this->guard)->user();
        if( !empty($user) ){
            $favorite_courses = $this->favorite_course_model->getAllPaginate($user->id);
            $has_courses = false;
            foreach( $favorite_courses as $course ){
                if( $course->user_id == $user->id ){
                    $has_courses = true;
                }
            }
            return view('general.course.favorite.list',
                [
                    'user'      => $user,
                    'courses'   => $favorite_courses,
                    'has_courses' => $has_courses
                ]
            );
        }else{
            abort(404);
        }
    }

    public function add(){
        $id = $_POST['id'];
        $user = Auth::guard($this->guard)->user();
        $course = $this->course_model->find($id);
        $favorite_course = FavoriteCourse::where('course_id', $id)->where('user_id', $user->id)->first();
        if( $course && empty($favorite_course) ){
            FavoriteCourse::create(['user_id' => $user->id, 'course_id' => $id]);
            return redirect('course/favorite');
        }else{
            return redirect()->back();
        }
    }

    public function remove(){
        $id = $_POST['id'];
        $user = Auth::guard($this->guard)->user();
        $favorite_course = FavoriteCourse::find($id);
        if( $favorite_course && $favorite_course->user_id == $user->id ){
            $favorite_course->delete($id);
            return redirect('course/favorite');
        }else{
            return redirect()->back();
        }
    }
}
