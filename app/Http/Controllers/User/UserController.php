<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Brackets\AdminAuth\Models\AdminUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserInformation;
use App\Http\Requests\User\StoreUserInformation;
use App\Models\AuthorRequest;

class UserController extends Controller
{

    protected $course_model;
    protected $guard;
    public function __construct()
    {
        $this->course_model = new Course();
        $this->guard = 'admin';
    }

    public function index(){
        return;
    }

    public function settings(){
        $user = Auth::guard($this->guard)->user();
        $user_information = UserInformation::where('user_id', $user->id)->get();
        $author_request_send = (AuthorRequest::where('user_id', $user->id)->count() == 1) ? true : false;
        return view('general.user.settings', ['user' => $user,'user_information'  => $user_information, 'author_request_send' => $author_request_send]);
    }

    public function show($user_id)
    {
        $user = AdminUser::where('id', $user_id)->first();
        if( $user && $user->hasAnyRole(['User', 'Author'])){
            $courses_author = null;
            $courses_teacher = null;
            $user_information = UserInformation::where('user_id', $user_id)->get();
            return view('general.user.detail',
                [
                    'user'              => $user,
                    'courses_author'    => $courses_author,
                    'courses_teacher'   => $courses_teacher,
                    'user_information'  => $user_information
                ]);
        }else{
            abort(404);
        }
    }

    function storeInformation(StoreUserInformation $request){
        $request->merge([
            'user_id'   => Auth::guard($this->guard)->user()->id
        ]);

        // Sanitize input
        $sanitized = $request->getSanitized();

        UserInformation::create($sanitized);
        return redirect('user/settings');
    }

    function deleteInformation($item_id){
        $user_information = UserInformation::find($item_id);
        $user_information->delete($item_id);
        return redirect('user/settings');
    }


}
