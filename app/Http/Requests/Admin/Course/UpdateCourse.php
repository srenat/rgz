<?php

namespace App\Http\Requests\Admin\Course;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCourse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.course.edit', $this->course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            // 'slug' => ['required', Rule::unique('courses', 'slug'), 'string'],
            'description' => ['nullable', 'string'],
            'certificate' => ['nullable', 'string'],
            'hours' => ['required', 'integer'],
            'contacts' => ['nullable', 'string'],
            'published' => ['required', 'boolean'],
            'created_by' => ['nullable', 'integer'],
            'modified_by' => ['nullable', 'integer'],
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {

        // $sanitized = $this->validated();

        $validator = \Validator::make($this->all(), [
            'name' => ['required', 'string'],
            'description' => ['nullable', 'string'],
            'author' => ['nullable', 'exists:admin_users,id'],
            'teacher' => ['nullable', 'exists:admin_users,id'],
            'certificate' => ['nullable', 'string'],
            'hours' => ['required', 'integer'],
            'contacts' => ['nullable', 'string'],
            // 'slug' => ['required', Rule::unique('courses', 'slug'), 'string'],
            'created_by' => ['nullable', 'integer'],
            'modified_by' => ['nullable', 'integer'],
            'published_at' => ['nullable', 'date'],
            'published' => ['required', 'boolean'],
            'approved'  => ['required', 'boolean'],
            'type'   => ['required', 'exists:course_types,id'],
        ]);

        $sanitized = $validator->validate();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
