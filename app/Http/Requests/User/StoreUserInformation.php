<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserInformation extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'value' => ['required', 'string'],
            // 'user_id' => ['required', 'integer']
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        // $sanitized = $this->validated();

        $validator = \Validator::make($this->all(), [
            'name' => ['required', 'string'],
            'value' => ['required', 'string'],
            'user_id' => ['required', 'integer', 'exists:admin_users,id']
        ]);

        $sanitized = $validator->validate();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
