<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthorRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id'
    ];


    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

}
