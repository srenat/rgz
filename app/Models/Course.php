<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Permission\Traits\HasRoles;

class Course extends Model implements HasMedia
{
    use SoftDeletes;
    use HasRoles;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;
    use ProcessMediaTrait;

    protected $fillable = [
        'name',
        'description',
        'slug',
        'certificate',
        'hours',
        'contacts',
        'published',
        'approved',
        'created_by',
        'modified_by',
    ];


    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',

    ];

    protected $appends = ['resource_url'];

    public function registerMediaCollections() : void
    {
        $this->addMediaCollection('preview')
                ->accepts('image/*')
                ->canUpload('admin.upload');

    }

    public function registerMediaConversions(Media $media = null) : void
    {
        $this->autoRegisterThumb200();

        $this->addMediaConversion('detail_hd')
            ->width(1920)
            ->height(1080)
            ->performOnCollections('preview');
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/courses/'.$this->getKey());
    }

    public function type()
    {
        return $this->belongsToMany('App\Models\CourseType', 'course_type', 'course_id', 'type_id');
    }

    public function author()
    {
        return $this->belongsToMany('Brackets\AdminAuth\Models\AdminUser', 'course_author', 'course_id', 'author_id');
    }

    public function teacher()
    {
        return $this->belongsToMany('Brackets\AdminAuth\Models\AdminUser', 'course_teacher', 'course_id', 'teacher_id');
    }



    /**
     * Get all courses paginate
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllPaginate($limit = 9)
    {
        return $this->where('published', 1)->where('approved', 1)->orderBy('created_at', 'desc')->paginate($limit);
    }

    /**
     * Get course bu slug
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCourseBySlug( $slug ){
        return $this->where('published', 1)->where('approved', 1)->where('slug', $slug)->first();
    }

    /**
     * Get all courses paginate by query
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getPaginateWithQuery($query, $limit = 9)
    {
        return $this->where('published', 1)
                    ->where('approved', 1)
                    ->where(function($q) use ($query){
                        $q->where('name', 'like', '%'.$query.'%')
                            ->orWhere('description', 'like', '%'.$query.'%')
                            ->orWhereHas('author', function ($subquery) use ($query){
                                $subquery->where('first_name', 'like', '%'.$query.'%')
                                        ->orWhere('last_name', 'like', '%'.$query.'%');
                            })
                            ->orWhereHas('teacher', function ($subquery) use ($query){
                                $subquery->where('first_name', 'like', '%'.$query.'%')
                                        ->orWhere('last_name', 'like', '%'.$query.'%');
                            })
                            ->orWhereHas('type', function ($subquery) use ($query){
                                $subquery->where('name', 'like', '%'.$query.'%');
                            });
                    })
                    ->orderBy('created_at', 'desc')
                    ->paginate($limit);
    }
}
