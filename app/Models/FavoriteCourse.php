<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FavoriteCourse extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'course_id',
    ];

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function user()
    {
        return $this->belongsTo('Brackets\AdminAuth\Models\AdminUser');
    }

    /**
     * Get all favorite courses paginate
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllPaginate($user_id, $limit = 9)
    {
        return $this
            ->with(
                ['user' => function ($q) use ($user_id){
                $q->where('id', $user_id);
                }, 'course' => function ($q){
                $q->where('published', 1)->where('approved', 1)->orderBy('created_at', 'desc');
            }])
            ->with('course.type')
            ->paginate($limit);
    }

}
