<?php

namespace App\Models;

use Brackets\AdminAuth\Models\AdminUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_informations';

    protected $fillable = [
        'name',
        'value',
        'user_id',
    ];

    public function user(){
        return $this->belongsTo(AdminUser::class);
    }

}
