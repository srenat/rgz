import AppListing from '../app-components/Listing/AppListing';

Vue.component('author-request-listing', {
    mixins: [AppListing]
});