import AppListing from '../app-components/Listing/AppListing';

Vue.component('course-type-listing', {
    mixins: [AppListing]
});