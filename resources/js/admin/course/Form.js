import AppForm from '../app-components/Form/AppForm';

Vue.component('course-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                slug:  '' ,
                description:  '' ,
                author_id:  '' ,
                teacher_id:  '' ,
                certificate:  '' ,
                contacts:  '' ,
                published:  false ,
                created_by:  '' ,
                modified_by:  '' ,
            },
            mediaCollections: ['preview']
        }
    }

});
