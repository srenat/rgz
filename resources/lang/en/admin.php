<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',

            //Belongs to many relations
            'roles' => 'Roles',

        ],
    ],

    'course' => [
        'title' => 'Courses',

        'actions' => [
            'index' => 'Courses',
            'create' => 'New Course',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'type_id' => 'Type',
            'author_id' => 'Author',
            'teacher_id' => 'Teacher',
            'image' => 'Image',
            'published' => 'Published',
            'created_by' => 'Created by',
            'modified_by' => 'Modified by',

        ],
    ],

    'course' => [
        'title' => 'Courses',

        'actions' => [
            'index' => 'Courses',
            'create' => 'New Course',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'description' => 'Description',
            'type_id' => 'Type',
            'author_id' => 'Author',
            'teacher_id' => 'Teacher',
            'certificate' => 'Certificate',
            'contacts' => 'Contacts',
            'image' => 'Image',
            'published' => 'Published',
            'created_by' => 'Created by',
            'modified_by' => 'Modified by',

        ],
    ],

    'course' => [
        'title' => 'Courses',

        'actions' => [
            'index' => 'Courses',
            'create' => 'New Course',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'description' => 'Description',
            'type_id' => 'Type',
            'author_id' => 'Author',
            'teacher_id' => 'Teacher',
            'certificate' => 'Certificate',
            'contacts' => 'Contacts',
            'published' => 'Published',
            'created_by' => 'Created by',
            'modified_by' => 'Modified by',

        ],
    ],

    'course-type' => [
        'title' => 'Course Types',

        'actions' => [
            'index' => 'Course Types',
            'create' => 'New Course Type',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',

        ],
    ],

    // Do not delete me :) I'm used for auto-generation

    'admin_panel' => 'Admin Panel'
];
