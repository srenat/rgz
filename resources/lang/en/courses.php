<?php
    return [
        'new_courses'   => 'New courses',
        'certificates'  => 'Certificates',
        'contacts'      => 'Contacts',
        'author'        => 'Course Author',
        'teacher'       => 'Course Teacher',
        'duration'      => 'Duration',
        'hours'         => 'hours',
        'as_author'     => 'As Author',
        'as_teacher'    => 'As Teacher',
        'search'        => 'Search',
        'not_found'     => 'Not found',
        'courses'       => 'Courses',
        'favorite'      => 'Favorite',
        'favorite_add'  => 'Add to favorite',
        'favorite_remove' => 'Remove from favorite'
    ];
