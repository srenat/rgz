<?php
    return [
        'roles'             => 'Roles',
        'Administrator'     => 'Administrator',
        'User'              => 'User',
        'Author'            => 'Author'
    ];
