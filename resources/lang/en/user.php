<?php
    return [
        'settings' => 'Settings',
        'phone'    => 'Phone',
        'site'     => 'Site',
        'email'    => 'Email',
        'add'      => 'Add',
        'delete'   => 'Delete',
        'field_value' => 'Field value',
        'field_type'    => 'Field type',
        'author_request' => 'Author request',
        'get_author_access' => 'Get Author access',
        'author_request_was_send'  => 'Author request was send'
    ];
