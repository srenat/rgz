<div class="card">
    <div class="card-header">
        <i class="fa fa-bars"></i>{{ trans('admin.type.columns.type_id') }}
    </div>
    <div class="card-block">

        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('type'), 'has-success': fields.type && fields.type.valid }">
            <div class="col-md-12">
                <multiselect v-model="form.type" :options="{{ $types->toJson() }}" placeholder="{{ trans('admin.type.columns.type_id') }}" label="name" track-by="id"></multiselect>
                <div v-if="errors.has('type')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('type') }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <i class="fa fa-bars"></i>{{ trans('admin.course.columns.hours') }}
    </div>
    <div class="card-block">
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('hours'), 'has-success': fields.hours && fields.hours.valid }">
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-12 col-xl-12'">
                <input type="number" v-model="form.hours" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('hours'), 'form-control-success': fields.hours && fields.hours.valid}" id="hours" name="hours" placeholder="{{ trans('admin.course.columns.hours') }}">
                <div v-if="errors.has('hours')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('hours') }}</div>
            </div>
        </div>
    </div>
</div>
@if( Auth::guard('admin')->user()->can('admin.upload') )
    <div class="card">
        @if($edit)
            @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => app(App\Models\Course::class)->getMediaCollection('preview'),
                'media' => $course->getThumbs200ForCollection('preview'),
                'label' => trans('admin.course.preview')
            ])
        @else
            @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => app(App\Models\Course::class)->getMediaCollection('preview'),
                'label' => trans('admin.course.preview')
            ])
        @endif
    </div>
@endif
