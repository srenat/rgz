<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.course.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

{{-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('slug'), 'has-success': fields.slug && fields.slug.valid }">
    <label for="slug" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.slug') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.slug" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('slug'), 'form-control-success': fields.slug && fields.slug.valid}" id="slug" name="slug" placeholder="{{ trans('admin.course.columns.slug') }}">
        <div v-if="errors.has('slug')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('slug') }}</div>
    </div>
</div> --}}

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('description'), 'has-success': fields.description && fields.description.valid }">
    <label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.description') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.description" v-validate="''" id="description" name="description" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description') }}</div>
    </div>
</div>

@if( Auth::guard('admin')->user()->can('admin.course') && Auth::guard('admin')->user()->can('admin') )
    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('author_id'), 'has-success': fields.author_id && fields.author_id.valid }">
        <label for="author_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.author_id') }}</label>

        <div class="col-md-9 col-xl-8">
            <multiselect v-model="form.author" :options="{{ $authors->toJson() }}" placeholder="{{ trans('admin.author_id.columns.id') }}" label="full_name" track-by="id"></multiselect>
            <div v-if="errors.has('author_id')" class="form-control-feedback form-text" v-cloak>
                @{{ errors.first('author_id') }}
            </div>
        </div>
    </div>
@endif

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('teacher_id'), 'has-success': fields.teacher_id && fields.teacher_id.valid }">
    <label for="teacher_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.teacher_id') }}</label>

    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.teacher" :options="{{ $teachers->toJson() }}" placeholder="{{ trans('admin.teacher_id.columns.id') }}" label="full_name" track-by="id"></multiselect>
        <div v-if="errors.has('teacher_id')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('teacher_id') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('certificate'), 'has-success': fields.certificate && fields.certificate.valid }">
    <label for="certificate" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.certificate') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.certificate" v-validate="''" id="certificate" name="certificate" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('certificate')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('certificate') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('contacts'), 'has-success': fields.contacts && fields.contacts.valid }">
    <label for="contacts" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.contacts') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.contacts" v-validate="''" id="contacts" name="contacts" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('contacts')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('contacts') }}</div>
    </div>
</div>

<div class="form-check row" :class="{'has-danger': errors.has('published'), 'has-success': fields.published && fields.published.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="published" type="checkbox" v-model="form.published" v-validate="''" data-vv-name="published"  name="published_fake_element">
        <label class="form-check-label" for="published">
            {{ trans('admin.course.columns.published') }}
        </label>
        <input type="hidden" name="published" :value="form.published">
        <div v-if="errors.has('published')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('published') }}</div>
    </div>
</div>

@if( Auth::guard('admin')->user()->can('admin.course') && Auth::guard('admin')->user()->can('admin') )
    <div class="form-check row" :class="{'has-danger': errors.has('approved'), 'has-success': fields.approved && fields.approved.valid }">
        <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
            <input class="form-check-input" id="approved" type="checkbox" v-model="form.approved" v-validate="''" data-vv-name="approved"  name="approved_fake_element">
            <label class="form-check-label" for="approved">
                {{ trans('admin.course.columns.approved') }}
            </label>
            <input type="hidden" name="approved" :value="form.approved">
            <div v-if="errors.has('approved')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('approved') }}</div>
        </div>
    </div>
@endif

{{-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_by'), 'has-success': fields.created_by && fields.created_by.valid }">
    <label for="created_by" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.created_by') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.created_by" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('created_by'), 'form-control-success': fields.created_by && fields.created_by.valid}" id="created_by" name="created_by" placeholder="{{ trans('admin.course.columns.created_by') }}">
        <div v-if="errors.has('created_by')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_by') }}</div>
    </div>
</div> --}}

{{-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('modified_by'), 'has-success': fields.modified_by && fields.modified_by.valid }">
    <label for="modified_by" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.modified_by') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.modified_by" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('modified_by'), 'form-control-success': fields.modified_by && fields.modified_by.valid}" id="modified_by" name="modified_by" placeholder="{{ trans('admin.course.columns.modified_by') }}">
        <div v-if="errors.has('modified_by')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('modified_by') }}</div>
    </div>
</div> --}}


