@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.course.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">

        <course-form
            :action="'{{ url('admin/courses') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>

                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.course.actions.create') }}
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus"></i> {{ trans('admin.article.actions.create') }}
                            </div>
                            <div class="card-body">
                                @include('admin.course.components.form-elements', ['edit' => false])
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        @include('admin.course.components.form-elements-right', ['edit' => false])
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>

            </form>

        </course-form>

        </div>

        </div>


@endsection
