@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.course.actions.edit', ['name' => $course->name]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <course-form
                :action="'{{ $course->resource_url }}'"
                :data="{{ $course->toJson() }}"
                v-cloak
                inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.course.actions.edit', ['name' => $course->name]) }}
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-8">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-plus"></i> {{ trans('admin.article.actions.create') }}
                                </div>
                                <div class="card-body">
                                    @include('admin.course.components.form-elements', ['edit' => true])
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            @include('admin.course.components.form-elements-right', ['edit' => true])
                        </div>
                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

        </course-form>

        </div>

</div>

@endsection
