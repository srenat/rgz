@extends('layouts.app')

@section('content')
<div class="container px-0">
    <div class="col-12">
        <h3> {{ $course->name }} </h3>
    </div>
    <div class="container row justify-content-start">
        <div class="col-12">
            {!! $course->description !!}
        </div>

        <div class="col-12">
            <h5> {{ __('courses.certificates') }}</h5>
            {!! $course->certificate !!}
        </div>


        <div class="col-12">
            <h5> {{ __('courses.contacts') }}</h5>
            {!! $course->contacts !!}
        </div>


        <div class="col-12">
            <h5>{{ __('courses.duration')}} </h5>
                {{ $course->hours }} {{ __('courses.hours') }}
        </div>


        @if( $course->author && !empty($course->author[0]) )
            <div class="col-12">
                <h5> {{ __('courses.author') }}</h5>
                <a href="/user/{{$course->author[0]->id}}">
                    {{ $course->author[0]->full_name }}
                </a>
            </div>
        @endif

        @if( $course->teacher && !empty($course->teacher[0]) )
            <div class="col-12">
                <h5> {{ __('courses.teacher') }}</h5>
                <a href="/user/{{$course->teacher[0]->id}}">
                    {{ $course->teacher[0]->full_name }}
                </a>
            </div>
        @endif
    </div>
</div>
@endsection
