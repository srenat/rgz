@extends('layouts.app')

@section('content')
<div class="container">
    <h3>{{ __('courses.new_courses') }}</h3>
    @if( !empty($user))
        <a class="btn btn-success mb-2" href="{{ route('course.favorite.list') }}">
            {{ __('courses.favorite') }}
        </a>
    @endif
    <form action="{{ route('course.list')}}" method="GET">
        <div class="input-group mb-3">
            <input type="text" name="query" value="@if( isset($_GET['query']) && !empty($_GET['query'])){{$_GET['query']}}@endif" class="form-control" placeholder="{{ __('courses.search') }}" aria-label="{{ __('courses.search') }}" aria-describedby="button-addon2">
            <div class="input-group-append">
                <input type="submit" class="btn btn-outline-secondary"  id="button-addon2" value="{{ __('courses.search') }}">
            </div>
        </div>
    </form>
    @if( $courses->count() )
        <div class="courses">
            @foreach ($courses as $course)
                <div class="course__item">
                    <a class="course" href="course/{{ $course->slug }}">
                        <div class="course__name">
                            {{ $course->name }}
                        </div>
                        <div class="course__type">
                            {{ $course->type[0]->name }}
                        </div>

                        <div class="course__bg">
                            <img src="{{ $course->getFirstMediaUrl('preview') }}"/>
                        </div>
                    </a>
                    @if( !empty($user))
                        <form action="{{ route('course.favorite.add') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{ $course->id }}">
                            <input type="submit" class="btn btn-success w-100" value="{{ __('courses.favorite_add') }}">
                        </form>
                    @endif
                </div>
            @endforeach
        </div>
    @else
        <h5>{{ __('courses.not_found') }}</h5>
    @endif
    <div class="my-3">
        {{ $courses->links() }}
    </div>

</div>
@endsection
