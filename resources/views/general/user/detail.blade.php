@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card justify-content-center w-100 my-2">
            @if( !empty($user->getFirstMediaUrl('avatar')) )
                <img class="card-img-top" src="{{ $user->getFirstMediaUrl('avatar') }}" alt="{{ $user->full_name }}">
            @endif
            <div class="card-body">
                <h5 class="card-title">{{ $user->full_name }}</h5>
                <div class="py-2">
                    @foreach ($user->roles as $role)
                        <span class="alert alert-primary mr-2" role="alert">
                            {{ __( 'roles.'.$role->name ) }}
                        </span>
                    @endforeach
                </div>
                <div class="py-3">
                    @foreach ($user_information as $item)
                        <div class="alert alert-secondary">
                            <span class="mr-2">{{  __('user.'.$item->name) }}:</span>
                            @if($item->name == 'site' )
                                <a href="{{ $item->value }}" class="mr-5">{{ $item->value }}</a>
                            @else
                                <span class="mr-5">{{ $item->value }}</span>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        {{-- @if( $user->hasRole('Author'))
            <div class="card justify-content-center w-100 my-2">
                <div class="card-body">
                    <h5 class="card-title">{{ __('courses.as_author') }}</h5>
                </div>
            </div>
        @endif
        @if( $user->hasRole('User'))
            <div class="card justify-content-center w-100 my-2">
                <div class="card-body">
                    <h5 class="card-title">{{ __('courses.as_teacher') }}</h5>
                </div>
            </div>
        @endif --}}
    </div>
</div>
@endsection
