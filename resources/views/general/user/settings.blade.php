@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        <h5>{{ $user->full_name }}</h5>
        <div class="card w-100 py-2 mt-2">
            <div class="p-2">
                @foreach ($user->roles as $role)
                    <span class="alert alert-primary mr-2" role="alert">
                        {{ __( 'roles.'.$role->name ) }}
                    </span>
                @endforeach
            </div>
                @if( $author_request_send )
                    <div class="p-2">
                        {{ __('user.author_request_was_send') }}
                    </div>
                @else
                    @if( !$user->hasRole('Author'))
                    <div class="p-2">
                        <form action="{{ route('author.request') }}" method="POST">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                            <input type="submit" class="btn btn-primary" value="{{ __('user.author_request') }}">
                        </form>
                    </div>
                    @endif
                @endif
        </div>

        @if( !empty($user_information) )
            <div class="card w-100 py-2 mt-2">
                <div class="card-body">
                    @foreach ($user_information as $item)
                        <div class="d-flex form-group align-items-center">
                            <div class="col-6">
                                <span class="mr-2">{{  __('user.'.$item->name) }}:</span>
                                <span class="mr-5">{{ $item->value }}</span>
                            </div>
                            <form class="col-6" action="{{ url('user/settings/'.$item->id) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-danger" :disabled="submiting">
                                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                    {{  __('user.delete') }}
                                </button>
                            </form>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="card w-100 py-2 mt-2">
            <form action="{{ url('user/settings') }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group align-items-center">
                        <label for="formControlSelect">{{ __('user.field_type') }}</label>
                        <select name="name" class="form-control" id="formControlSelect">
                            <option value="phone">
                                {{ __('user.phone') }}
                            </option>
                            <option value="site">
                                {{ __('user.site') }}
                            </option>
                            <option value="email">
                                {{ __('user.email') }}
                            </option>
                        </select>
                    </div>
                    <div class="form-group align-items-center">
                        <label for="formControlText">{{ __('user.field_value') }}</label>
                        <input type="text" class="form-control" name="value" id="formControlText" required>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{  __('user.add') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
