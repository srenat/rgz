<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});

Route::middleware(['web', 'is.admin', 'auth:' . config('admin-auth.defaults.guard')])->group(static function () {
    Route::namespace('Brackets\AdminAuth\Http\Controllers')->group(static function () {
        Route::get('/admin', 'AdminHomepageController@index')->name('brackets/admin-auth::admin');
    });
});



// Auth::routes();

Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'index'])->name('login');
Route::post('/login', [Brackets\AdminAuth\Http\Controllers\Auth\LoginController::class, 'login']);
Route::any('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::get('/register', [App\Http\Controllers\Auth\RegisterController::class, 'index'])->name('register');
Route::post('/register',  [App\Http\Controllers\Auth\RegisterController::class, 'store'])->name('store');


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/',[App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth:' . config('admin-auth.defaults.guard')])->group(static function() {
    Route::prefix('user')->group( static function(){
        Route::get('/',                 [App\Http\Controllers\User\UserController::class, 'index'])->name('user');
        Route::get('/settings',         [App\Http\Controllers\User\UserController::class, 'settings'])->name('user-settings');
        Route::post('/settings',        [App\Http\Controllers\User\UserController::class, 'storeInformation'])->name('store-information');
        Route::post('/settings/{information_id}',        [App\Http\Controllers\User\UserController::class, 'deleteInformation'])->name('delete-information');

    });
});

Route::prefix('user')->group( static function(){
    Route::get('/{user_id}',        [App\Http\Controllers\User\UserController::class, 'show'])->name('show');
});

Route::prefix('author')->group( static function(){
    Route::post('/request',        [App\Http\Controllers\Author\AuthorController::class, 'request'])->name('author.request');
});

Route::prefix('course')->group( static function(){
    Route::get('/',                     [App\Http\Controllers\Course\CourseController::class, 'index'])->name('course.list');
    Route::get('/search',               [App\Http\Controllers\Course\CourseController::class, 'search'])->name('course.search');
    Route::get('/favorite',             [App\Http\Controllers\Course\FavoriteCourseController::class, 'index'])->name('course.favorite.list');
    Route::get('/{slug}',               [App\Http\Controllers\Course\CourseController::class, 'show'])->name('course.detail');
    Route::post('/add',                 [App\Http\Controllers\Course\FavoriteCourseController::class, 'add'])->name('course.favorite.add');
    Route::post('/remove',              [App\Http\Controllers\Course\FavoriteCourseController::class, 'remove'])->name('course.favorite.remove');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard')/*, 'admin'*/])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('courses')->name('courses/')->group(static function() {
            Route::get('/',                                             'CoursesController@index')->name('index');
            Route::get('/create',                                       'CoursesController@create')->name('create');
            Route::post('/',                                            'CoursesController@store')->name('store');
            Route::get('/{course}/edit',                                'CoursesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CoursesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{course}',                                    'CoursesController@update')->name('update');
            Route::delete('/{course}',                                  'CoursesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('course-types')->name('course-types/')->group(static function() {
            Route::get('/',                                             'CourseTypesController@index')->name('index');
            Route::get('/create',                                       'CourseTypesController@create')->name('create');
            Route::post('/',                                            'CourseTypesController@store')->name('store');
            Route::get('/{courseType}/edit',                            'CourseTypesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CourseTypesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{courseType}',                                'CourseTypesController@update')->name('update');
            Route::delete('/{courseType}',                              'CourseTypesController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('author-requests')->name('author-requests/')->group(static function() {
            Route::get('/',                                             'AuthorRequestsController@index')->name('index');
            Route::post('/{id]',                                            'AuthorRequestsController@getAuthorAccess')->name('access');
        });
    });
});

Route::middleware(['auth:' . config('admin-auth.defaults.guard')])->group(static function () {
    Route::namespace('Brackets\Media\Http\Controllers')->group(static function () {
        Route::post('upload', 'FileUploadController@upload')->name('brackets/media::upload');
        Route::get('view', 'FileViewController@view')->name('brackets/media::view');
    });
});
